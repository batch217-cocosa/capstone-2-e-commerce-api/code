const express = require("express");
const router = express.Router();
const User = require("../models/User.js");
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")




router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.get("/details", auth.verify ,(req, res) => {

	const user = {
		id : auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	userController.getUserDetails(user).then(resultFromController => res.send(resultFromController));
})

// Get all Users (Admin Only)
router.get("/all", auth.verify, (req, res) => {

	const adminAccess = auth.decode(req.headers.authorization).isAdmin


	userController.getAllUser(adminAccess).then(resultFromController => res.send(resultFromController));
})


// Summarize Cart
router.post("/checkout",auth.verify, (req, res) => {

	const adminAccess = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin,
		id : auth.decode(req.headers.authorization).id
	}

	userController.checkout(adminAccess, req.body).then(resultFromController => res.send(resultFromController));
})




module.exports = router;