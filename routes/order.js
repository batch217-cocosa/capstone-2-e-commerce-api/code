const express = require("express");
const router = express.Router();
const Order = require("../models/Order.js");
const User = require("../models/User.js");
const orderController = require("../controllers/orderController.js");
const userController = require("../controllers/userController.js");
const auth = require ("../auth.js")

// Will return user cart ID or create one if user is newly registered
router.get("/", auth.verify, (req, res) => {

	const user = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		id: auth.decode(req.headers.authorization).id 
	}

	orderController.getCart(user).then(resultFromController => res.send(resultFromController));
})

// Will add products from req.body to Order, will check if new product or add to existing product
router.post("/addtocart", auth.verify,(req,res) => {

	const user = {
		id : auth.decode(req.headers.authorization).id,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}


	orderController.addToCart(user, req.body).then(resultFromController => res.send(resultFromController));
})

// Will get prices from product, calculate subtotal per product and total amount, and return all order details 
router.post("/cart", auth.verify, (req, res) => {

	const user = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		id: auth.decode(req.headers.authorization).id 
	}

	orderController.cartSummary(user).then(resultFromController => res.send(resultFromController));
})


router.post("/checkout", auth.verify, (req, res) => {

	const user = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		id: auth.decode(req.headers.authorization).id 
	}

	orderController.checkout(user).then(resultFromController => res.send(resultFromController));
})


router.get("/:orderId", auth.verify, (req, res) => {

	

	orderController.getOrder(req.params.orderId).then(resultFromController => res.send(resultFromController));
})


router.patch("/:orderId/complete", auth.verify, (req,res)=> {
	const user = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		id: auth.decode(req.headers.authorization).id 
	}

	orderController.orderComplete(user, req.params.orderId).then(resultFromController => res.send(resultFromController));
})



module.exports = router;