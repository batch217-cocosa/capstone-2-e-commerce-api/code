const express = require("express");
const router = express.Router();
const Product = require("../models/Product.js");
const productController = require("../controllers/productController.js");
const auth = require ("../auth.js")

// Add new product
router.post("/add", auth.verify,(req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}


	productController.createProduct(data).then(resultFromController => {
		res.send(resultFromController)
	})
})



// See all product
router.get("/all", (req, res) => {

	const adminAccess = auth.decode(req.headers.authorization).isAdmin

	productController.getAllProduct(adminAccess).then(resultFromController => res.send(resultFromController));
})


// See all active products
router.get("/", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
})

router.get("/inactive", (req, res) => {

	const adminAccess = auth.decode(req.headers.authorization).isAdmin 

	productController.getInactiveProducts(adminAccess).then(resultFromController => res.send(resultFromController));
})


// Get single product
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController));
})




// Edit product details
router.patch("/:productId/update", auth.verify, (req, res) => {

	const adminAccess = auth.decode(req.headers.authorization).isAdmin 

	productController.updateProduct(adminAccess, req.params.productId, req.body).then(resultFromController => res.send(resultFromController));
})




// Archive inactive products
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const adminAccess = auth.decode(req.headers.authorization).isAdmin 

	productController.archiveProduct(adminAccess, req.params.productId).then(resultFromController => res.send(resultFromController));
})

// Archive inactive products
router.patch("/:productId/unarchive", auth.verify, (req, res) => {

	const adminAccess = auth.decode(req.headers.authorization).isAdmin 

	productController.UnarchiveProduct(adminAccess, req.params.productId).then(resultFromController => res.send(resultFromController));
})

module.exports = router;