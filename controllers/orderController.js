const Order = require("../models/Order")
const Product = require("../models/Product")
const User = require("../models/User")

// Will return user cart ID or create one if user is newly registered
module.exports.getCart = async (user) => {

	if (user.isAdmin){

		return false
	} 

	
	let currentCart = await User.findById(user.id).then(result => {


		if (result.openCart != undefined && result.openCart != '' ){

			console.log(result.openCart)
			let cart = result.openCart;
			
			return cart

		}


		let newCart = new Order({ purchasedBy : user.id});
		console.log("Error on new Order")

		let orderCheck =  newCart.save().then((newCart, err) => {
					
			if (err){
				return false
			}
			console.log("Error on save")
			return true
		})


		if (orderCheck){
					
			return User.findByIdAndUpdate(user.id, {

				openCart : newCart._id

			})
			.then ((updatedUser, err) => {
				if (err){
					return false
				} 
				return true
				console.log("error on ordercheck")
			})
		} else {
			return false
		}
	})
	
	return currentCart
}

// Will add products from req.body to Order, will check if new product or add to existing product
module.exports.addToCart = async (user, product) => { 

	if(user.isAdmin){
		return false
	}

	let orderId = await User.findById(user.id).then(cartId => {
		return cartId.openCart
	})

	let isAdded = await Order.findByIdAndUpdate(orderId).then(cart => {

		let nonexist = true;

		for (let i = 0; i < cart.orderedProducts.length; i++ ){
			if (cart.orderedProducts[i].productId == product.productId){

				cart.orderedProducts[i].quantity += product.quantity || 1;

				nonexist = false
			}
		}

		if (nonexist){
			cart.orderedProducts.push(product);
			cart.orderedProducts[cart.orderedProducts.length-1].quantity = 1 ;
		}
			
		return cart.save().then((order, err) => {
			if(err){
				return false
			} else {
				return true
			}
		})
	})
}

// Will get prices from product, calculate subtotal per product and total amount, and return all order details 
module.exports.cartSummary = async (user) => {

	

	let total=0;
	let subtotal=0;

	let userCart = await User.findById(user.id).then( async cartId => {

		let cartUpdated = await Order.findById(cartId.openCart).then( async cart => {

			for ( let i = 0; i < cart.orderedProducts.length; i++){

				let computation = await Product.findById(cart.orderedProducts[i].productId).then(result => {
					subtotal = result.price * cart.orderedProducts[i].quantity;

					cart.orderedProducts[i].subtotal = subtotal;

					total += subtotal; 

				})

			}
			cart.totalAmount = total;

			return cart.save().then((res, err) => {
				if (err){
					return err
				}
				return res
			})


		})

		return cartUpdated

	})

	return userCart

}

// Will change cart status from "open" to "ongoing", and place order ID from openCart to orderOngoing

module.exports.checkout = async ( user ) => {

	if (user.isAdmin){
		return "Normal User Access Only"
	}

	let currentCart = await User.findByIdAndUpdate(user.id).then( async result => {

		if( result.openCart != null){

			let transfer = await Order.findByIdAndUpdate(result.openCart,{

				status : "ongoing",
				purchasedOn: new Date()

			}).then((cartUpdate, err) => {
					
				if (err){
					return err
				}
					return true
			})
			console.log(transfer)
			if(transfer){
				result.orderOngoing.push(result.openCart);

				result.openCart = "";

			}
			console.log(result)
			return result.save().then((userUpdate, err) => {
				if (err){
					return err
				}
				return true
			})
		}
	})

}

module.exports.getOrder = (orderId) => { 
	return Order.findById(orderId).then(result => {
		return result
	})

}

module.exports.orderComplete = async (user, orderId) => {
	
	if (user.isAdmin){

		let userChange = Order.findById(orderId).then( async result=> {
			
			let userComplete =  await User.findById(result.purchasedBy).then(async res=> {

				res.orderComplete.push(orderId);
			
				for (let i = 0; i < res.orderOngoing.length; i++){
					
					if (orderId == res.orderOngoing[i]){ 

				 		res.orderOngoing.splice(i,1);
				 

						 return res.save().then((saveUser, error)=>{
				 			if (error){
				 				return error
				 			} else {
				 				return true
				 		}
					 })
				}
			}

			return false

		})

			
			return userComplete
			

			})
		
		if (userChange){
		let changeStatus =  await Order.findByIdAndUpdate(orderId,{
			status: "complete"
		})
		.then((orderDone, err) =>{
			if (err){
				return false
			}
			return true
			})
		}
	}

	else {
		return false
	}
}	