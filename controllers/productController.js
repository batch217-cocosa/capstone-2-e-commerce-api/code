const Product = require("../models/Product")

module.exports.createProduct = (data) => {
	if (data.isAdmin){
		let newProduct = new Product ({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			quantity: data.quantity,
			imagesrc: data.imagesrc
		})

		return newProduct.save().then((newProduct, err) => {
			if (err){
				return err
			}

			return true
		})

	} 

	else {
		return false
	}
}

module.exports.getAllProduct = (admin) => {
	if (admin){

		return Product.find({}).then(result => {
			return result
		})
	}
	return false
}


module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result
	})
}

module.exports.getInactiveProducts = (admin) => {

	if(admin){
		return Product.find({isActive: false}).then(result => {
			return result
		})
	}

	return false
}


module.exports.getProduct = (productId) => { 
	return Product.findById(productId).then(result => {
		return result
	})

}


module.exports.updateProduct = (admin, productId, newData) => {
	if (admin){
		return Product.findByIdAndUpdate(productId, {
			name: newData.name,
			description: newData.description,
			price: newData.price,
			quantity: newData.quantity,
			imagesrc: newData.imagesrc
		})
		.then((updatedProduct, err) => {
			if (err){
				return false
			} 
			return true
		})
	}
	else {
		let message = Promise.resolve('Admin access only.')

		return message.then((value) => {
			return value 
		})
	}
}

module.exports.archiveProduct = (admin, productId) => {
	if (admin){
		return Product.findByIdAndUpdate(productId, {
			isActive: false
		})
		.then((archivedProduct, err) => {
			if (err){
				return false
			}
			return true
		})
	}
}

module.exports.UnarchiveProduct = (admin, productId) => {
	if (admin){
		return Product.findByIdAndUpdate(productId, {
			isActive: true
		})
		.then((UnarchivedProduct, err) => {
			if (err){
				return false
			}
			return true
		})
	}
}



