const User = require("../models/User.js");
const Order = require("../models/Order.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10)

	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true;
		}
	})
}

module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {

		if(!result) return false
		

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){

				return  {access : auth.createAccessToken(result)}
			} else {

				return false
			}
	})
}


module.exports.getUserDetails = (user) => { 

	return User.findById(user.id).then(result => {
		return result
		
	})

}

module.exports.getAllUser = (admin) => {
	if (admin){
		return User.find({}).then(result => {
			return true
		})
	}
	else {

		let message = Promise.resolve('Admin access only.')

		return message.then((value) => {
			return true 
		})
	}
}



module.exports.checkout = async (userAccess) => {

	if(userAccess.isAdmin){
		return "User Access Only"
	}

	let userCart = await User.findbyId(userAccess.id).then(result => {
			return result.openCart
	})

	
		
	

}


