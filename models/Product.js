const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required : [true, "Product Name is Required!"]
	},
	description: {
		type: String,
		required: [true, "Description is Required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is Required!"]
	},
	quantity:{
		type: Number
	},
	imagesrc:{
		type: String
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
		isActive: {
		type: Boolean,
		default: true
	}
})

module.exports = mongoose.model("Product", productSchema);