const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({

	purchasedBy: {
		type: String
	},

	status: {
		type: String,
		default: "open"
	},

	orderedProducts: [{
		productId: {
			type: String
		},
		quantity: {
			type: Number
		},
		subtotal: {
			type: Number
			
		}
	}],

	totalAmount: {
		type: Number
	},

	purchasedOn: {
		type: Date
	}
})

module.exports = mongoose.model("Order", orderSchema);