const mongoose = require("mongoose");

		const userSchema = new mongoose.Schema({
			firstName: {
				type: String,
				required: [true, "Enter your first name"]
			},
			lastName: {
				type: String,
				required: [true, "Enter your last name"]
			},
			mobileNo: {
				type: Number
			},
			email : {
				type : String,
				required : [true, "Email is required"]
			},
			password : {
				type : String,
				required : [true, "Password is required"]
			},
			isAdmin : {
				type : Boolean,
				default : false
			},
			openCart: {
				type: String
			},

			orderOngoing:{
				type: Array
			},

			orderComplete : {
				type : Array
			}

		})

		module.exports = mongoose.model("User", userSchema);

